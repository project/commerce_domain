<?php

namespace Drupal\commerce_domain\MachineName\Field;

/**
 * Holds machine names of Order entity fields.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class Order {

  /**
   * Holds the domain that the order is associated with.
   */
  const DOMAIN = 'domain';

}
