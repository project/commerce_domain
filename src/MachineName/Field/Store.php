<?php

namespace Drupal\commerce_domain\MachineName\Field;

/**
 * Holds machine names of Store entity fields.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class Store {

  /**
   * Holds the domains that the store is associated with.
   */
  const DOMAINS = 'domains';

}
