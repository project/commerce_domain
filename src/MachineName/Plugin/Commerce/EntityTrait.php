<?php

namespace Drupal\commerce_domain\MachineName\Plugin\Commerce;

/**
 * Holds IDs of Entity Trait plugins.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class EntityTrait {

  /**
   * Holds the ID of the plugin that enables domain tracking for orders.
   */
  const ORDER_DOMAIN_TRACKER = 'commerce_domain_order_domain_tracker';

}
