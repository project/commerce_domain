<?php

namespace Drupal\commerce_domain_order\Plugin\Commerce\EntityTrait;

use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;
use Drupal\commerce_domain\MachineName\Field\Order as OrderField;
use Drupal\entity\BundleFieldDefinition;

/**
 * Trait that enables domain tracking on orders.
 *
 * @CommerceEntityTrait(
 *   id = "commerce_domain_order_domain_tracker",
 *   label = @Translation("Track the domain for orders of this type"),
 *   entity_types = {"commerce_order"}
 * )
 */
class OrderDomainTracker extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $field_definition = BundleFieldDefinition::create('entity_reference')
      ->setLabel('Domain')
      ->setDescription('The domain that the order is associated with.')
      ->setCardinality(1)
      ->setRequired(FALSE)
      ->setSetting('target_type', 'domain')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return [OrderField::DOMAIN => $field_definition];
  }

}
