<?php

namespace Drupal\commerce_domain_cart\EventSubscriber;

use Drupal\commerce_cart\CartSessionInterface;
use Drupal\commerce_domain\MachineName\Field\Order as OrderField;
use Drupal\commerce_domain\MachineName\Plugin\Commerce\EntityTrait;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sets the domain that the order was placed on.
 *
 * @I Track other events, such as when a cart is created
 *    type     : improvement
 *    priority : normal
 *    labels   : order
 */
class OrderDomainTracker implements EventSubscriberInterface {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The cart session.
   *
   * @var \Drupal\commerce_cart\CartSessionInterface
   */
  protected $cartSession;

  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * The order type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $orderTypeStorage;

  /**
   * Constructs a new OrderDomainTracker object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $account_proxy
   *   The current user account proxy.
   * @param \Drupal\commerce_cart\CartSessionInterface $cart_session
   *   The cart session.
   * @param \Drupal\domain\DomainNegotiatorInterface $domain_negotiator
   *   The domain negotiator.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    AccountProxyInterface $account_proxy,
    CartSessionInterface $cart_session,
    DomainNegotiatorInterface $domain_negotiator,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->account = $account_proxy->getAccount();
    $this->cartSession = $cart_session;
    $this->domainNegotiator = $domain_negotiator;
    $this->orderTypeStorage = $entity_type_manager
      ->getStorage('commerce_order_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      // We need to set the domain before the cart is finalized so that we have
      // correct information on whether the order is a cart.
      'commerce_order.place.pre_transition' => ['setDomain', 100],
    ];
    return $events;
  }

  /**
   * Sets the domain that the order was placed on.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   *
   * @I Improve checkout detection on order domain tracking
   *    type     : bug
   *    priority : normal
   *    labels   : order
   */
  public function setDomain(WorkflowTransitionEvent $event) {
    $order = $event->getEntity();
    $order_type = $this->orderTypeStorage->load($order->bundle());
    if (!$order_type->hasTrait(EntityTrait::ORDER_DOMAIN_TRACKER)) {
      return;
    }

    // If the order is placed via the admin UI by an order manager, we do not
    // want to programmatically set the domain because the user might be acting
    // on a domain different than the intended.
    // For now, we set the domain only if the customer places the order via the
    // checkout process. We have no better way to detect that other than to
    // check that the order is a cart order and the current user is the
    // customer. This does leave us with the edge case of an order manager
    // placing an order through the admin UI for oneself that has marked it as
    // cart.
    // For orders placed via the admin UI, the domain field should become
    // editable in the form display.
    if (!$order->get('cart')->value) {
      return;
    }
    if (!$this->userIsCustomer($order)) {
      return;
    }

    $order->set(
      OrderField::DOMAIN,
      $this->domainNegotiator->getActiveDomain()
    );
  }

  /**
   * Returns whether the current user is the order's customer.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   *   TRUE if the user is the order's customer, FALSE otherwise.
   */
  protected function userIsCustomer(OrderInterface $order) {
    $authenticated = $this->account->isAuthenticated();
    if ($authenticated) {
      return $order->getCustomerId() == $this->account->id();
    }

    // If the user is anonymous, we need to verify that the cart is in the user
    // session; otherwise it could belong to another anonymous user.
    return $this->cartSession->hasCartId($order->id());
  }

}
