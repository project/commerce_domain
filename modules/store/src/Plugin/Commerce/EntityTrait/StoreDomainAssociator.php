<?php

namespace Drupal\commerce_domain_store\Plugin\Commerce\EntityTrait;

use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;
use Drupal\commerce_domain\MachineName\Field\Store as StoreField;
use Drupal\entity\BundleFieldDefinition;

use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Trait that enables store-to-domain association.
 *
 * @CommerceEntityTrait(
 *   id = "commerce_domain_store_domain_associator",
 *   label = @Translation("Allow associating stores of this type with domains"),
 *   entity_types = {"commerce_store"}
 * )
 */
class StoreDomainAssociator extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $field_definition = BundleFieldDefinition::create('entity_reference')
      ->setLabel('Domains')
      ->setDescription('The domains that the store is associated with.')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(FALSE)
      ->setSetting('target_type', 'domain')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return [StoreField::DOMAINS => $field_definition];
  }

}
